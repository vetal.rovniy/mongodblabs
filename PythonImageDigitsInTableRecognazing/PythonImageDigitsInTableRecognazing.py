import tensorflow as tf
import numpy as np
import cv2

model = tf.keras.models.load_model('C:/study/my_model.h5')

# Load the image with the table
image = cv2.imread('C:/study/table5.png')

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)# Preprocess the image by thresholding and applying morphological operations

_, thresh = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

# Apply morphological operations to remove noise
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
morph = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)

# Find the contours in the image
contours, _ = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Loop over each contour and extract the digit inside it
for cnt in contours:
    x,y,w,h = cv2.boundingRect(cnt)
    digit = thresh[y:y+h, x:x+w]
    digit = cv2.resize(digit, (28, 28))

    # Reshape the digit to match the input shape of the model
    digit = np.reshape(digit, (1, 28, 28, 1))

    # Normalize the pixel values to be between 0 and 1
    digit = digit / 255.0

    # Use the model to predict the digit
    pred = model.predict(digit)
    digit_pred = np.argmax(pred)

    # Print the predicted digit
    print("Predicted digit:", digit_pred)