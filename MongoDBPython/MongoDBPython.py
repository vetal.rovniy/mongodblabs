#database access
import pymongo
client = pymongo.MongoClient('mongodb://localhost:27017/')
db = client["test"]
collection = db["rovnyi6DB"]

result = collection.insert_one({'name': 'John', 'age': 30})
print(result.inserted_id)

#insert documents

post_data = {
'Title': 'Python and MongoDB',
'Content': 'PyMongo is fun, you guys',
'Author': 'Scott'
}
result = collection.insert_one (post_data)
print ( 'One post: {0}'. format (result.inserted_id))

post_1 = {
'Title': 'Python and MongoDB',
'Content': 'PyMongo is fun, you guys',
'Author': 'Scott'
}
post_2 = {
'Title': 'Virtual Environments',
'Content': 'Use virtual environments, you guys',
'Author': 'Scott'
}
post_3 = {
'Title': 'Learning Python',
'Content': 'Learn Python, it is easy',
'Author': 'Bill'
}
new_result = collection.insert_many ([post_1, post_2, post_3])
print ( 'Multiple posts: {0}'. format (new_result.inserted_ids))

#take the documets

bills_post = collection.find_one ({ "Author": "Bill"})
print (bills_post)


scotts_posts = collection.find ({ "author": "Scott"})
print (scotts_posts)
#<pymongo.cursor.Cursor object at 0x000001FDC38B3D90>


scotts_posts = collection.find ({ "Author": "Scott"})
for doc in scotts_posts:print (doc)

#close open session for client
client.close


#MongoEngine

import Post
import PostObject
import Author

from mongoengine import *
disconnect_all
connect ( "mongoengine_test", host = "localhost", port = 27017)

post_1 = Post.Rovnyi6DB_F(
title = "Sample Post",
content = "Some engaging content",
author = "Dingo"
)
post_1.save() # This will perform an insert
print (post_1.title)
post_1.title = "A Better Post Title222"
post_1.save() # This will perform an atomic edit on "title"
print (post_1.title)


post_2 = Post.Rovnyi6DB_F(content = 'Content goes here', author = 'Michael')
try:
    post_2.save()
    print('Person saved successfully')
except ValidationError as e:
    print(f'Error saving person: {e}')
except Exception as e:
    print(f'Error saving person: {e}')
    
#raise ValidationError (message, errors = errors)
#mongoengine.errors.ValidationError:ValidationError(Post: None) (Field is required: [ 'title'])

# object-oriented functions
auth = Author.Author(name = "ScottNew")
auth.save()

post_3 = PostObject.Rovnyi6DB(
title = "Sample Post for object",
content = "Some engaging content object",
author = Author.Author.objects.first()
)
post_3.save() # This will perform an insert
print (post_3.title)
post_3.title = "A Better Post Title with object"
post_3.save() # This will perform an atomic edit on "title"
print (post_1.title)

author = PostObject.Rovnyi6DB.objects.first().author
print(author.name)




