import Author 
from mongoengine import Document, StringField, BooleanField, queryset_manager, ReferenceField
class Rovnyi6DB(Document):
    title = StringField()
    published = BooleanField()
    content = StringField()
    author = ReferenceField(Author.Author)
    @queryset_manager
    def live_posts (clazz, queryset):
        return queryset.filter (published = True)




