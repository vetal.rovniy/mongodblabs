﻿import numpy as np
from sklearn.datasets import make_classification
#from sklearn.neural_network import HopfieldNetwork
from hopfieldnetwork import HopfieldNetwork
from sklearn.neural_network import MLPRegressor

# Define the sigmoid activation function
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# Define the neural network model
def neyron(x1, x2, w1, w2, w3, w4, w5, w6):
    # Calculate hidden layer activations
    h1_input = w1*x1 + w2*x2
    h1_output = sigmoid(h1_input)
    h2_input = w3*x1 + w4*x2
    h2_output = sigmoid(h2_input)
    
    # Calculate output layer activation
    o1_input = w5*h1_output + w6*h2_output
    o1_output = sigmoid(o1_input)
    
    return o1_output

# Set up inputs and weights
x1 = 0.5
x2 = 0.8
w1 = -0.2
w2 = 0.4
w3 = 0.3
w4 = 0.1
w5 = 1.2
w6 = -0.5

# Calculate the output of the neural network
output = neyron(x1, x2, w1, w2, w3, w4, w5, w6)

print(output)

#2build Hopfield and Elman neural networks
#generating data samples for a simple classification problem using the scikit-learn library:
X, y = make_classification(n_samples=100, n_features=5, n_classes=2, random_state=42)

#Elman neural network
model = MLPRegressor(hidden_layer_sizes=(10,), activation='logistic', solver='lbfgs')
model.fit(X, y)

#у випадку мережі Елмана нам потрібно вказати кількість нейронів у прихованому шарі за допомогою параметра hidden_layer_sizes, 
#а також функцію активації за допомогою параметра activation. Параметр solver визначає алгоритм оптимізації, який буде використано.

#Hopfield neural network
# create an instance of the HopfieldNetwork class with 4 neurons
network = HopfieldNetwork(4)

# define the patterns to be memorized
pattern1 = np.array([1, 1, 1, 1])
pattern2 = np.array([-1, -1, 1, -1])

# store the patterns in the network
network.store_patterns([pattern1, pattern2])

# retrieve a pattern from the network
output = network.run(np.array([1, -1, 1, -1]))

print(output)


