import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

#convert a vector to a binary matrix:
def vector_to_binary_matrix(vec):
    # convert the vector to a matrix with one column
    mat = np.reshape(vec, (-1, 1))
    
    # get the maximum value in the matrix
    max_val = np.max(mat)
    
    # calculate the number of bits required to represent the maximum value
    num_bits = int(np.ceil(np.log2(max_val + 1)))
    
    # use numpy's binary_repr function to convert each element to binary representation
    # and concatenate the binary strings to form the binary matrix
    bin_mat = np.array([list(np.binary_repr(x, width=num_bits)) for x in mat]).astype(int)
    
    return bin_mat

#return unique rows of a matrix:
def unique_rows(matrix):
    # convert the matrix to a one-dimensional array of tuples
    arr = np.ascontiguousarray(matrix).view(np.dtype((np.void, matrix.dtype.itemsize * matrix.shape[1])))
    
    # use numpy's unique function to get the unique rows
    _, idx = np.unique(arr, return_index=True)
    unique_mat = matrix[idx]
    
    return unique_mat

#fill a matrix with random numbers from a normal distribution, calculate the mean and variance of each column, and plot histograms of each row:

def random_matrix_stats(M, N):
    # Generate a matrix with random numbers from a normal distribution
    mat = np.random.normal(size=(M, N))
    
    # Calculate the mean and variance of each column
    col_means = np.mean(mat, axis=0)
    col_vars = np.var(mat, axis=0)
    
    # Plot histograms of each row
    fig, axs = plt.subplots(M, 1, figsize=(8, M*2))
    for i in range(M):
        axs[i].hist(mat[i], bins=20)
        axs[i].set_xlabel('Value')
        axs[i].set_ylabel('Frequency')
        axs[i].set_title('Row {}'.format(i+1))
    
    plt.tight_layout()
    plt.show()
    
    return col_means, col_vars

# create a tensor representing an image of a circle with a given color and radius on a black background:
# color parameter should be a tuple of three integers representing the RGB values of the desired color, e.g. (255, 0, 0) for red.
def create_circle_image(radius, color):
    # Create a black background image
    img = np.zeros((radius*2, radius*2, 3), dtype=np.uint8)
    
    # Create a circle mask
    Y, X = np.ogrid[:img.shape[0], :img.shape[1]]
    dist_from_center = np.sqrt((X - radius)**2 + (Y-radius)**2)
    mask = dist_from_center <= radius
    
    # Color the circle in the image with the given color
    img[mask] = color
    
    return img

# select a part of a fixed-size matrix centered on an element:
def select_centered_submatrix(matrix, size):
    """
    Selects a part of a fixed-size matrix centered on a given element.

    Parameters:
    matrix (ndarray): A 2D numpy array representing the input matrix.
    size (int): The size of the submatrix.

    Returns:
    ndarray: A 2D numpy array representing the submatrix.
    """
    assert size % 2 == 1, "Size must be odd"
    row, col = np.divmod(np.argmax(matrix), matrix.shape[1])
    r_start = max(row - size // 2, 0)
    r_end = min(row + size // 2 + 1, matrix.shape[0])
    c_start = max(col - size // 2, 0)
    c_end = min(col + size // 2 + 1, matrix.shape[1])
    return matrix[r_start:r_end, c_start:c_end]

#standardize all values of a tensor:
def standardize_tensor(tensor):
    """
    Standardizes all values of a tensor.

    Parameters:
    tensor (ndarray): A 3D numpy array representing the input tensor.

    Returns:
    ndarray: A 3D numpy array representing the standardized tensor.
    """
    tensor_mean = tensor.mean()
    tensor_std = tensor.std()
    standardized_tensor = (tensor - tensor_mean) / tensor_std
    return standardized_tensor

# Main program 
def main():

    # Load data from CSV file
    data = np.genfromtxt('C:/study/data4.csv', delimiter=';',  dtype=int)

    # Split data into matrices and vectors
    matrices = data[:, :16].reshape(-1, 4, 4)
    vectors = data[:, 16:].reshape(-1, 4, 1)

    # Calculate products and sum
    products = matrices * vectors
    result = np.sum(products, axis=0)

    # Save the result to a CSV file
    np.savetxt('C:/study/result.csv', result, delimiter=',', fmt="%d")


if __name__ == "__main__":main()