﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MongoDBConsole
{
    [BsonIgnoreExtraElements]
    public class Person
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }
        [BsonElement("First Name")]
        public string Name { get; set; }
        [BsonIgnore]
        public string Surename { get; set; }

        [BsonRepresentation(BsonType.String)]
        [BsonIgnoreIfDefault]
        public int Age { get; set; }
        [BsonIgnoreIfNull]
        public Company Company { get; set; }
        public List<string> Languages { get; set; }

    }

    [BsonIgnoreExtraElements]
    public class Company
    {     
        public string Name { get; set; }
    }
}
