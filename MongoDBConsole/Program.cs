﻿
//using System.Configuration;
/*
string connString = "mongodb://localhost:27017";
MongoClient client = new MongoClient(connString);
IMongoDatabase database = client.GetDatabase("rovnyi6K");
*/

using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using MongoDBConsole;
using System.Drawing.Text;
using System.Threading.Tasks.Sources;

var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", false).Build();

MongoClient client = new MongoClient(configuration.GetConnectionString("DataConnection"));


GetDataBaseNames(client).GetAwaiter();

Console.ReadLine();


GetCollectionsNames(client).GetAwaiter();

Console.ReadLine();

//Отримання всіх БД з сервера
static async Task GetDataBaseNames(MongoClient client)
{
    using (var cursor = await client.ListDatabasesAsync())
    {
        var databaseDocuments = await cursor.ToListAsync();
        foreach (var databaseDocument in databaseDocuments)
        {
            Console.WriteLine(databaseDocument["name"]);
        }
    }
}

static async Task GetCollectionsNames(MongoClient client)
{
    using (var cursor = await client.ListDatabasesAsync())
    {
        var dbs = await cursor.ToListAsync();
        foreach (var db in dbs)
        {
            Console.WriteLine("In database {0} created next collections:", db["name"]);
            IMongoDatabase database = client.GetDatabase(db["name"].ToString());

            using (var collCursor = await database.ListCollectionsAsync())
            {
                var colls = await collCursor.ToListAsync();
                foreach (var coll in colls)
                {
                    Console.WriteLine(coll["name"]);
                }
            }

            Console.WriteLine();
        }
    }
}


/*********/

IMongoDatabase db = client.GetDatabase("rovnyi6K");
IMongoCollection<BsonDocument> colls = db.GetCollection<BsonDocument>("rovnyi6K");
var docs = await colls.Find(_ => true).ToListAsync();

foreach (var doc0 in docs)
{ 
    Console.WriteLine(doc0);
}
Console.ReadLine();


//BsonDocument
BsonDocument doc = new BsonDocument { { "name", "BiilyJagger" } };
Console.WriteLine(doc);
Console.WriteLine(doc["name"]);

doc["name"] = "Urich";
Console.WriteLine(doc.GetValue("name"));
Console.ReadLine();


/******/

BsonDocument doc1 = new BsonDocument().Add("name", "Tristan");
Console.WriteLine(doc1);
Console.ReadLine();


BsonDocument doc2 = new BsonDocument { {"name","Dingo"},{ "surename","Ford"},{"age",new BsonInt32(48)},
    { "company",new BsonDocument { {"name","apple" },{"year",new BsonInt32(1974) },{ "price",new BsonInt32(300000)} } 
    } 
};
Console.WriteLine(doc2);
Console.ReadLine();


/********/

BsonDocument chemp = new BsonDocument();
chemp.Add("countries",new BsonArray(new[] { "Brasil","Argentina","Germany","Niderlands"}));
chemp.Add("finished", new BsonBoolean(true));
Console.WriteLine(chemp);
Console.ReadLine();


/***************/

Person person = new Person { Name="Robert",Surename="Goblin", Age = 55};
person.Company = new Company { Name = "Dingo" };
Console.WriteLine(person.ToJson());
Console.ReadLine();


BsonDocument doc3 = new BsonDocument { {"Name","Dingo"},{ "Surename","Ford"},{"Age",new BsonInt32(48)},
    { "Company",new BsonDocument { {"Name","apple" }}
    }
};
Person pers = BsonSerializer.Deserialize<Person>(doc);
Console.WriteLine(pers.ToJson());
Console.ReadLine();


Person pers1 = new Person { Name="Retro",Surename="Pepelats",Age=99};
pers.Company = new Company { Name = "Volvo" };

BsonDocument doc4 = pers1.ToBsonDocument();
Console.WriteLine(doc);
Console.ReadLine();



Person pers2 = new Person { Name = "Retro", Surename = "Pepelats", Age = 99 };
pers2.Company = new Company { Name = "Volvo" };
Console.WriteLine(pers2.ToJson());
Console.ReadLine();


/*Ігнорування значень за замовчуванням*/

Person pers3 = new Person {Name="Frans",Surename="Kafka",Age=57 };
Console.WriteLine(pers3.ToJson());
Console.ReadLine();

/**/

/*
 BsonRepresentation
 BsonClassMap
*/
/*
BsonClassMap.RegisterClassMap<Person>(
    cm =>
    {
        cm.AutoMap();
        cm.MapMember(p => p.Name).SetElementName("name");
    });
Person person4 = new Person { Name="Dingo2",Age=566};
BsonDocument doc5 = person4.ToBsonDocument();
Console.WriteLine(doc5.ToString());
Console.ReadLine();
*/

/*Конвенці*/

var conventionPack = new ConventionPack();
conventionPack.Add(new CamelCaseElementNameConvention());
ConventionRegistry.Register("camelCase", conventionPack,t=> true);
Person person5 = new Person { Name = "Dingo2", Age = 56 };

BsonDocument doc6 = person5.ToBsonDocument();
Console.WriteLine(doc6.ToString());
Console.ReadLine();

/*Збереження документів в базу даних*/

SaveDoc().GetAwaiter().GetResult();
Console.ReadLine();
static async Task SaveDoc()
{
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    var colls = database.GetCollection<BsonDocument>("rovnyi6K");

    BsonDocument person1 = new BsonDocument
    {
        {"Name", "BSOD" },
        {"Age",99},
        {"Language", new BsonArray{"english","ukrainian"} }
    };

    BsonDocument person2 = new BsonDocument
    {
        {"Name", "BSOD" },
        {"Age",99},
        {"Language", new BsonArray{"english","ukrainian"} }
    };
    BsonDocument person3 = new BsonDocument
    {
        {"Name", "BSOD" },
        {"Age",99},
        {"Language", new BsonArray{"english","ukrainian"} }
    };

    await colls.InsertOneAsync(person1);
    await colls.InsertManyAsync(new[] { person2, person3 });
}

SaveDoc2().GetAwaiter().GetResult();
Console.ReadLine();

static async Task SaveDoc2()
{
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    var colls = database.GetCollection<Person>("rovnyi6K");

    Person person1 = new Person
    {
        Name = "BSOD",
        Age =99,
        Languages = new List<string> { "english","ukrainian"},
        Company = new Company { Name = "Ford"}
    };

    Person person2 = new Person
    {
        Name = "BSOD",
        Age = 99,
        Languages = new List<string> { "english", "ukrainian" },
        Company = new Company { Name = "Ford" }
    };

    Person person3 = new Person
    {
        Name = "BSOD",
        Age = 99,
        Languages = new List<string> { "english", "ukrainian" },
        Company = new Company { Name = "Ford" }
    };

    await colls.InsertOneAsync(person1);
    await colls.InsertManyAsync(new[] { person2, person3 });
}


/*Вибірка з бази даних
 Фільтрація даних
 */
FindDocs().GetAwaiter().GetResult();
Console.ReadLine();

static async Task FindDocs()
{
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    var colls = database.GetCollection<BsonDocument>("rovnyi6K");
    //empty filter
    //var filter = new BsonDocument();
    /**/
    var filter = new BsonDocument("$or", new BsonArray 
    {
        new BsonDocument("Age", new BsonDocument("$gte",33)),
        new BsonDocument("Name","Tom")
    }
    );
    /*
    var people = colls.Find(filter).ToListAsync();
    foreach(var p in people)
    {
        
    }
    */
    using (var curs = await colls.FindAsync(filter))
    {
        while (await curs.MoveNextAsync()) 
        {
            var people = curs.Current;
            foreach (var doc in people)
            {
                Console.WriteLine(doc);
            }
        }

    }
}
Console.ReadLine();

/*Вибірка з бази даних 
 Фільтрація даних*/
FindPeople().GetAwaiter().GetResult();
Console.ReadLine();

static async Task FindPeople()
{
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    var colls = database.GetCollection<BsonDocument>("rovnyi6K");

    var filter = new BsonDocument("$or", new BsonArray
    {
        new BsonDocument("Age", new BsonDocument("$gte",33)),
        new BsonDocument("Name","Tom")
    });

    /*
     var filter = new BsonDocument("$and", new BsonArray
    {
        new BsonDocument("Age", new BsonDocument("$gte",33)),
        new BsonDocument("Name","Tom")
    });
     */

    var people = await colls.Find(filter).ToListAsync();
    foreach(var p in people)
    {
        Console.WriteLine(p);
    }

}

Console.ReadLine();

/*Вибірка з бази даних 
 Фільтрація даних*/
FindPeople2().GetAwaiter().GetResult();
Console.ReadLine();

static async Task FindPeople2()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");

    //1
    
    var colls = database.GetCollection<BsonDocument>("rovnyi6K");
    var filter = Builders<BsonDocument>.Filter.Eq("Name", "Bill");
    var people = await colls.Find(filter).ToListAsync();

    //2
    /*
    var colls = database.GetCollection<Person>("rovnyi6K");
    var filter = Builders<Person>.Filter.Eq("Name", "Bill");
    var people = await colls.Find(filter).ToListAsync();
    */
    //3
    /*
    var builder = Builders<BsonDocument>.Filter;
    //var filter = builder.Eq("Name","Sunny") | builder.Eq("Name", "John");
    var filter = builder.Eq("Age", 40) &! builder.Eq("Name", "John");
    var people = await colls.Find(filter).ToListAsync();
    */

    //4
    /*
    var colls = database.GetCollection<Person>("rovnyi6K");
    var people = await colls.Find(x=>x.Name=="Bill" && x.Surename=="Own").ToListAsync();
    */


    //Логічні операції
    //5
    /*
    var filter1 = Builders<Person>.Filter.Eq("Name","Billy");
    var filter2 = Builders<Person>.Filter.Eq("Age", 25);
    var filterOr = Builders<Person>.Filter.Or(new List<FilterDefinition<Person>> { filter1, filter2 });
    var people = await colls.Find(filter).ToListAsync();
    */
    //6
    /*
    var filter3 = Builders<Person>.Filter.Eq("", "");
    var filter4 = Builders<Person>.Filter.Eq("", "");
    var filterAnd = Builders<Person>.Filter.And(new List<FilterDefinition<Person>> { filter3, filter4 });
    var people = await colls.Find(filter).ToListAsync();
    */

    //7 Операції з масивами
    //var filter = Builders<Person>.Filter.All("Languages", new List<string>() { "english", "italian" });
    //var people = await colls.Find(filter).ToListAsync();

    //8
    //var filter = Builders<Person>.Filter.Size("Languages",2);
    //var people = await colls.Find(filter).ToListAsync();


    foreach (var p in people)
    {
        Console.WriteLine(p);
        //4Console.WriteLine($"{0}-{1}",p.Name,p.Age);    
    }

}
Console.ReadLine();

//Інтерфейс IFindFluent і його методи

SortPeople().GetAwaiter().GetResult();
Console.ReadLine();
static async Task SortPeople()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    var colls = database.GetCollection<Person>("rovnyi6K");

    //1

    var people = await colls.Find(new BsonDocument()).Sort("{Age:" +
        "1}").ToListAsync();
    //var people = await colls.Find(new BsonDocument()).Sort("{Age:-1}").ToListAsync();
    
    //2 
    /*
    var res1 = await colls.Find(new BsonDocument()).SortByDescending(e => e.Name).ToListAsync();
    var res2 = await colls.Find(new BsonDocument()).SortBy(e => e.Name).ToListAsync();
    
    var sort = Builders<Person>.Sort.Ascending("Company.Name").Descending("Age");
    var people = await colls.Find(new BsonDocument()).Sort(sort).ToListAsync(); 
    */
    //3 Пропуск і лімітування вибірки:
    /* 
    var filter = new BsonDocument();
    var people = await colls.Find(filter).Skip(1).Limit(2).ToListAsync();
    */

    //4 Кількість документів вибірки
    /*
    var filter = new BsonDocument();
    long num = await colls.Find(filter).CountAsync();
    */
    //5 проекції
    //var people = await colls.Find(new BsonDocument()).Project("{Name:1,Age:1,_id:0}").ToListAsync();

    //6 застосування класу ProjectionDefinitionBuilder:
    /*
    var people = await colls.Find(new BsonDocument())
        .Project(Builders<Person>.Projection.Include("Name").Include("Age").Exclude("_id"))
        .ToListAsync();
    */

    foreach (var p in people)
    {
        Console.WriteLine(p);
    }

    //7 з Person в Employee:
    var filter = new BsonDocument();
    var projection = Builders<Person>.Projection.Expression(p => new Employee { Name = p.Name, Age = p.Age });
    var employees = await colls.Find(filter).Project<Employee>(projection).ToListAsync();

    foreach (var p in employees)
    {
        Console.WriteLine(p);
    }

}
Console.ReadLine();

//Агрегація і угруповання

AgregateAndFilter().GetAwaiter().GetResult();
Console.ReadLine();
static async Task AgregateAndFilter()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    var colls = database.GetCollection<Person>("rovnyi6K");

    //1 Угруповання
    /*
    var people = await colls.Aggregate()
        .Group(new BsonDocument { { "_id", "$Age" }, { "count", new BsonDocument("$sum", 1) } })
        .ToListAsync();
    foreach (var p in people)
    {
        Console.WriteLine($"{p.GetValue("_id")}-{p.GetValue("count")}");
    }
    */
    //2 Фільтрація
    /*
    var people = await colls.Aggregate()
        .Match(new BsonDocument { {"Name","Tom" },{"Company.name","Samsung" } }).ToListAsync();
    foreach (var p in people)
    {
        Console.WriteLine($"{p.Name}-{p.Age}");
    }
    */

    //3 Фільтрація
    var people = await colls.Aggregate()
        .Match(new BsonDocument { { "Name", "Tom" } })
        .Group(new BsonDocument { { "_id","$Company.name"},{"count", new BsonDocument("$sum",1) } })
        .ToListAsync();
    foreach (var p in people)
    {
        Console.WriteLine($"{p.GetValue("_id")}-{p.GetValue("count")}");
    }


}
Console.ReadLine();

//Редагування і видалення документів

EditDeleteDoc().GetAwaiter().GetResult();
Console.ReadLine();
static async Task EditDeleteDoc()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    var colls = database.GetCollection<BsonDocument>("rovnyi6K");
    var collsPerson = database.GetCollection<Person>("rovnyi6K");


    var res = await colls.ReplaceOneAsync(new BsonDocument("Name", "Jonny"),
        new BsonDocument
        {
            {"Name","Adam" },
            {"Age",100 },
            {"Languages",new BsonArray(new[]{"english","portugal" }) },
            {"Company",
                new BsonDocument{ { "Name","Microsoft"} }
            }
        });

    Console.WriteLine($"Find:{res.MatchedCount} updated:{res.ModifiedCount}");
    //2
    new UpdateOptions { IsUpsert = true };
    Console.WriteLine($"Upsert Id {res.UpsertedId}");    


    var people = await colls.Find(new BsonDocument()).ToListAsync();
    foreach (var p in people)
    {
        Console.WriteLine(p);
    }

    //3

    var res2 = await colls.UpdateOneAsync(
        new BsonDocument("Name","Tory"),
        new BsonDocument("$set",new BsonDocument("Age",28))
       // new BsonDocument("$inc", new BsonDocument("Age", 3))
        );

    Console.WriteLine($"Find:{res2.MatchedCount} updated:{res2.ModifiedCount}");

    //4
    var filter = Builders<BsonDocument>.Filter.Eq("Name", "Jonny");
    var update = Builders<BsonDocument>.Update.Set("Age", 99);
    //var update = Builders<BsonDocument>.Update.Set("Age", 99).CurrentDate("LastModified");

    var res3 = await colls.UpdateOneAsync(filter, update);
    //5
    var filter5 = Builders<BsonDocument>.Filter.Eq("Company.Name", "Apple");
    var update5 = Builders<BsonDocument>.Update.Set("Company.Name", "Google");
    var res5 = await colls.UpdateOneAsync(filter5, update5);
    //6
    var builder6 = Builders<BsonDocument>.Filter;
    var filter6 = builder6.Eq("Name", "Billy")&(builder6.Eq("Age",99));
    //var filter6 = builder6.Eq("Name", "Billy")|(builder6.Eq("Age",99));
    var update6 = Builders<BsonDocument>.Update.Set("Company.Name", "Google");
    var res6 = await colls.UpdateOneAsync(filter6, update6);
    //var res6 = await colls.UpdateManyAsync(filter6, update6);

    var peopleU = await colls.Find(new BsonDocument()).ToListAsync();
    foreach (var p in peopleU)
    {
        Console.WriteLine(p);
    }


    //delete
    var filterD = Builders<BsonDocument>.Filter.Eq("Name", "Tommy");
    await colls.DeleteOneAsync(filterD);
    await collsPerson.DeleteOneAsync(f=>f.Name == "Tommy");

    var peopleD = await colls.Find(new BsonDocument()).ToListAsync();
    foreach (var p in peopleD)
    {
        Console.WriteLine(p);
    }

    var resD = await collsPerson.DeleteManyAsync(p => p.Name == "Billy");
    Console.WriteLine($"Deleted {resD.DeletedCount}");

}

//Метод BulkWriteAsync
BulkWriteAsync().GetAwaiter().GetResult();
Console.ReadLine();
static async Task BulkWriteAsync()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    var colls = database.GetCollection<BsonDocument>("rovnyi6K");
    var collsPerson = database.GetCollection<Person>("rovnyi6K");

    var result = await colls.BulkWriteAsync(new WriteModel<BsonDocument>[]
        {
            new DeleteOneModel<BsonDocument>(new BsonDocument("Name","Fact"))
        });
    Console.WriteLine($"Deleted {result.DeletedCount}");

    var people = await colls.Find(new BsonDocument()).ToListAsync();
    foreach (var p in people)
    {
        Console.WriteLine(p);
    }

    //2

    var filter = Builders<BsonDocument>.Filter.Eq("Company.Name","Google");
    var update = Builders<BsonDocument>.Update.Set("Company.Name", "Apple");
    var newDoc = new BsonDocument {
        {"Name","Qwery" },
        {"Age",198 },
        {"Languages", new BsonArray{ "mssql","plpgsql"} },
        {"Company", new BsonDocument{ { "Name","Google"} }
        }};

    var res = await colls.BulkWriteAsync(new WriteModel<BsonDocument>[]
        {
            new DeleteOneModel<BsonDocument>(new BsonDocument("Name","Duda")),
            new UpdateOneModel<BsonDocument>(filter,update),
            new InsertOneModel<BsonDocument>(newDoc)
        });

    Console.WriteLine($"Deleted {res.DeletedCount}, added {res.InsertedCount}, updated {res.ModifiedCount}");

}


//Робота з GridFS
UploadFilesAsync().GetAwaiter().GetResult();
Console.ReadLine();
static async Task UploadFilesAsync()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    IGridFSBucket gridFs = new GridFSBucket(database);

    using (Stream fs = new FileStream("C:/study/table.png",FileMode.Open))
    {
        var id = await gridFs.UploadFromStreamAsync("table.png", fs);
        Console.WriteLine($"id upload file is:{id}");
    }

}

DownloadFilesAsync().GetAwaiter().GetResult();
Console.ReadLine();

static async Task DownloadFilesAsync()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    IGridFSBucket gridFs = new GridFSBucket(database);

    using (Stream fs = new FileStream("C:/study/table_new.png", FileMode.OpenOrCreate))
    {
        await gridFs.DownloadToStreamByNameAsync("table.png", fs);
    }

}

Console.ReadLine();

FindFilesAsync().GetAwaiter().GetResult();
Console.ReadLine();

static async Task FindFilesAsync()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    IGridFSBucket gridFs = new GridFSBucket(database);

    var filter = Builders<GridFSFileInfo>.Filter.Eq<string>(inf => inf.Filename, "table.png");
    var fileFind = await gridFs.FindAsync(filter);
    var fileInfo = fileFind.FirstOrDefault();
    Console.WriteLine($"id = {fileInfo.Id}");

}
Console.ReadLine();


ReplaceFilesAsync().GetAwaiter().GetResult();
Console.ReadLine();

static async Task ReplaceFilesAsync()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    IGridFSBucket gridFs = new GridFSBucket(database);

    using (Stream fs = new FileStream("table.png", FileMode.Open))
    {
        await gridFs.UploadFromStreamAsync("table_new.png", fs, new GridFSUploadOptions {Metadata = new BsonDocument("filename","table_new.png") });
    }

}
Console.ReadLine();

DeleteFilesAsync().GetAwaiter().GetResult();
Console.ReadLine();

static async Task DeleteFilesAsync()
{
    //common
    string connString = "mongodb://localhost:27017";
    MongoClient client = new MongoClient(connString);
    IMongoDatabase database = client.GetDatabase("rovnyi6K");
    IGridFSBucket gridFs = new GridFSBucket(database);

    var builder = new FilterDefinitionBuilder<GridFSFileInfo>();
    var filter = Builders<GridFSFileInfo>.Filter.Eq<string>(inf => inf.Filename, "table_new.png");
    var fileDels = await gridFs.FindAsync(filter);
    var fileInfo = fileDels.FirstOrDefault();
    await gridFs.DeleteAsync(fileInfo.Id);

}
Console.ReadLine();
