import numpy as np
import tensorflow as tf
from sklearn.datasets import make_blobs
from sklearn.datasets import make_regression
from sklearn.datasets import make_circles
import matplotlib.pyplot as plt

# generates a dataset with 100 samples, 2 classes, 2 features, and random state set to 42
# in this example, the synthetic forge data contains 2 features (n_features) and 100 points (n_samples)
X, y = make_blobs(n_samples=100, centers=2, n_features=2, random_state=42)

# Plot 1D distribution for the first feature
plt.hist(X[:,0], bins=20)

# Add plot title and axis labels
plt.title('1D distribution of the first feature')
plt.xlabel('Feature values')
plt.ylabel('Frequency')

# Show plot
plt.show()

# Generate synthetic regression data
X, y = make_regression(n_samples=100, n_features=1, noise=10)

# Plot the data
plt.scatter(X, y)
plt.title("Synthetic Regression Data")
plt.xlabel("X")
plt.ylabel("y")
plt.show()

# Generate synthetic data using make_blobs()
X, y = make_blobs(n_samples=1000, centers=3, random_state=42)

# Plot the data
plt.scatter(X[:, 0], X[:, 1], c=y, cmap='viridis')
plt.title("Clustering using make_blobs()")
plt.show()

# Generate synthetic data using make_circles()
X, y = make_circles(n_samples=1000, noise=0.05, random_state=42)

# Plot the data
plt.scatter(X[:, 0], X[:, 1], c=y, cmap='viridis')
plt.title("Clustering using make_circles()")
plt.show()

