import cv2
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, MaxPooling2D, Dropout
from keras.datasets import mnist
from keras.utils import to_categorical
import skimage #scikit-image

model = tf.keras.models.load_model('C:/study/mnist_model.h5')

#preparing the image
img_path = 'C:/study/table.png'

img = cv2.imread(img_path)
img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 7, 13)

#clear border
img = skimage.segmentation.clear_border(img)
img = cv2.bitwise_not(img)

_, thresh = cv2.threshold(img, 0, 512, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

# Find the contours in the image
contours,_ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

blank_image = np.zeros((30,30,3), np.uint8)
blank_image[:,:] = (255,255,255)
blank_image = cv2.cvtColor(blank_image,cv2.COLOR_BGR2GRAY)
blank_image = cv2.resize(blank_image,(28,28),cv2.INTER_CUBIC)

for cnt in contours:

    x,y,w,h = cv2.boundingRect(cnt)
    digit = img[y:y+h, x:x+w]
        
    height, width = digit.shape[:2]

    l_img = blank_image.copy()

    x_offset = y_offset = 1
    l_img[y_offset:y_offset+height, x_offset:x_offset+width] = digit.copy() 
        
    l_img = np.reshape(l_img, (1,28, 28,1))
   # Use the model to predict the digit
    pred = model.predict(l_img)
    digit_pred = np.argmax(pred)

    # Print the predicted digit
    print("Predicted digit:", digit_pred)

    print(l_img.shape)
    plt.imshow(np.reshape(l_img, (28, 28)), cmap='gray')
    plt.show()