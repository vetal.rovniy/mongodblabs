#img = Image.open('C:/study/img4.png').convert('L')

import tensorflow as tf
import numpy as np
from PIL import Image

# Load the MNIST dataset
mnist = tf.keras.datasets.mnist
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

# Preprocess the data
train_images = train_images / 255.0
test_images = test_images / 255.0

# Define the model architecture
model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(10, activation='softmax')
])

# Compile the model
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

# Train the model
model.fit(train_images, train_labels, epochs=10)

# Load a sample image of the digit 4
img = Image.open('C:/study/img4_2.png').convert('L')
img = img.resize((28, 28))

# Convert the image to a numpy array
img_array = np.array(img)

# Preprocess the image
img_array = img_array / 255.0

# Reshape the image to match the input shape of the model
img_array = img_array.reshape(1, 28, 28)

# Make a prediction on the image using the trained model
prediction = model.predict(img_array)

# Get the index of the highest probability class
predicted_class = np.argmax(prediction)

# Print the recognized digit
print("Recognized digit:", predicted_class)
