using MongoDBASPNET.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<ComputerContext>();
builder.Services.AddSingleton<Computer>();

var app = builder.Build();

app.MapGet("/", () => "Hello World!");

app.Run();
