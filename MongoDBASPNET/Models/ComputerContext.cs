﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Configuration;
using MongoDB.Driver.GridFS;

namespace MongoDBASPNET.Models
{
    public class ComputerContext
    {
        IMongoDatabase _database;
        IGridFSBucket _gridFS;
        private readonly IConfiguration _configuration;
        private readonly string _connString;

        public ComputerContext(IConfiguration configuration)
        {      
            
            _configuration = configuration;
            _connString = _configuration.GetConnectionString("DataConnection");

            var connection = new MongoUrlBuilder(_connString);
            MongoClient client = new MongoClient(_connString);
            _database = client.GetDatabase(connection.DatabaseName);
            _gridFS = new GridFSBucket(_database);

        }

        public IMongoCollection<Computer> Computers
        { 
            get { return _database.GetCollection<Computer>("Computers"); }
        }

        public async Task<IEnumerable<Computer>> GetComputers(int? year, string name)
        {
            var builder = new FilterDefinitionBuilder<Computer>();
            var filter = builder.Empty;
            if (!String.IsNullOrWhiteSpace(name))
            {
                filter = filter & builder.Regex("Name", new MongoDB.Bson.BsonRegularExpression(name));

            }
            if (year.HasValue)
            {
                filter = filter & builder.Eq("Year", year.Value);
            }
            return await Computers.Find(filter).ToListAsync();
        }

        public async Task<Computer> GetComputer(string id)
        {
            return await Computers.Find(new BsonDocument("_id", new ObjectId(id))).FirstOrDefaultAsync();
        }

        public async Task Create(Computer c)
        {
            await Computers.InsertOneAsync(c);
        }

        public async Task Update(Computer c)
        {
            await Computers.ReplaceOneAsync(new BsonDocument("_id",new ObjectId(c.Id)),c);
        }

        public async Task Remove(string id)
        {
            await Computers.DeleteOneAsync(new BsonDocument("_id", new ObjectId(id)));
        }

        public async Task<byte[]> GetImage(string id)
        {
            return await _gridFS.DownloadAsBytesAsync(new ObjectId(id));
        }

        public async Task StoreImage(string id, Stream stream, string imagName)
        {
            Computer c = await GetComputer(id);
            if(c!=null) 
            {
                if (c.HasImage != null)
                {
                    await _gridFS.DeleteAsync(new ObjectId(id));
                }
            }

            ObjectId imageId = await _gridFS.UploadFromStreamAsync(imagName, stream);
            c.ImageId = imageId.ToString();

            var filter = Builders<Computer>.Filter.Eq("_id",new ObjectId(c.Id));
            var update = Builders<Computer>.Update.Set("ImageId", c.ImageId);
            await Computers.UpdateOneAsync(filter, update);

        }


    }
}
