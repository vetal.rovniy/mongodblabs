﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Net.Http.Headers;

namespace MongoDBASPNET.Models
{
    public class Computer
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Display(Name="Model name")]
        public string Name { get; set; }

        [Display(Name = "Manufactured year")]
        public int Year { get; set; }

        public string ImageId {get; set;}

        public bool HasImage()
        {
            return !String.IsNullOrWhiteSpace(ImageId);
        }

    }
}
